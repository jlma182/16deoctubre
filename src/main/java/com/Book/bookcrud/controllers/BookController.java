package com.Book.bookcrud.controllers;
import com.Book.bookcrud.entities.Autor;
import com.Book.bookcrud.entities.Book;
import com.Book.bookcrud.repositories.AutorRepository;
import com.Book.bookcrud.services.AutorService;
import com.Book.bookcrud.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class BookController
{

    private BookService bookservice;
    private AutorService autorService;

    @Autowired
    private AutorRepository autorRepository;

    @Autowired
    public void setBookservice(BookService bookservice, AutorService autorService)
    {
        this.bookservice = bookservice;
        this.autorService = autorService;

    }

    @RequestMapping(value = "/books", method = RequestMethod.GET)
    public String index( Model model)
    {
        Iterable<Book> libros  = (List) bookservice.listAllBooks();
        model.addAttribute("visHtm", libros);
        return "books";
    }

    @RequestMapping(value = "/books/new", method = RequestMethod.GET)
    public String newPost(Model model)
    {
        Iterable<Autor> autores  = (List) autorService.listAllAutores();
        model.addAttribute("misAutores", autores);
        model.addAttribute("book",new Book());
        return "newBooks";
    }

    @RequestMapping(value = "/book", method = RequestMethod.POST)
    public String create(@ModelAttribute("book") Book libro, Model model)
    {


        bookservice.createBook(libro);

        
        return "redirect:/books";
    }

    @RequestMapping(value = "/booksh/{id}", method = RequestMethod.GET)
    public String show(@PathVariable Integer id, Model model)
    {
        Book book = bookservice.findBookById(id);
        //Book likes= bookservice.getlike(id);
        model.addAttribute("book", book);
        //model.addAttribute("like", book);
        return "show";
    }

    @RequestMapping(value = "/bookdel/{id}")
    public String deletebook(@PathVariable Integer id, Model model)
    {
        //Book libro = bookservice.findBookById(id);
        bookservice.deleteBook(id);
        return "redirect:/books";
    }

    @RequestMapping(value = "/bookedit/edit/{id}")
    public String edit(@PathVariable Integer id, Model model)
    {

        Iterable<Autor> autores  = (List) autorService.listAllAutores();
        model.addAttribute("misAutores", autores);

        Book book = bookservice.findBookById(id);
        model.addAttribute("book", book);
        return "editTitulo";
    }

   @RequestMapping(value = "/edit/book", method = RequestMethod.POST)
    public String update(@ModelAttribute("book") Book libro, Model model)
    {
        Book oldBook = bookservice.findBookById(libro.getId());
        oldBook.setTitulo(libro.getTitulo());
        oldBook.setCategoria(libro.getCategoria());

        oldBook.setAutorComboBox(libro.getAutorComboBox());

        bookservice.createBook(oldBook);
        return "redirect:/books";
    }

    @RequestMapping("/like/{id}")
    public String like(@PathVariable Integer id) {
        Book booklike = bookservice.findBookById(id);
        booklike.setLikes(booklike.getLikes()+1);
        bookservice.saveBook(booklike);
        return "redirect:/booksh/"+booklike.getId();
    }
}
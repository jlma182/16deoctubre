package com.Book.bookcrud.controllers;

import com.Book.bookcrud.entities.Autor;
import com.Book.bookcrud.services.AutorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class AutorController
{
    private AutorService autorservice;

    @Autowired
    public void setAutorService(AutorService autorservice)
    {
        this.autorservice=autorservice;
    }

    @RequestMapping (value = "/aut", method = RequestMethod.GET)
    public String index (Model model)
    {
        Iterable<Autor> autoress = (List) autorservice.listAllAutores();
        model.addAttribute("autores",autoress);
        return "authors";
    }

    @RequestMapping(value = "/autores/new", method = RequestMethod.GET)
    public String newAutor (Model model)
    {
        return "newAutor";
    }

   @RequestMapping(value = "/bookau",method = RequestMethod.POST)
    public String createautor(@ModelAttribute("book") Autor autores,Model model)
    {
        autorservice.createAutor(autores);
        return "redirect:/books";
    }

    @RequestMapping (value = "/autorsh/{id}", method = RequestMethod.GET)
    public String showautor(@PathVariable Integer id, Model model)
    {
        Autor shautor = autorservice.findAutorById(id);
        model.addAttribute("shautor",shautor);
        return "showautor";
    }

    @RequestMapping(value = "/autordel/{id}")
    public String deleteautor(@PathVariable Integer id, Model model)
    {
        autorservice.deleteAutor(id);
        return "redirect:/books";
    }

    @RequestMapping(value = "/autoredit/edit/{id}")
    public String edit(@PathVariable Integer id, Model model)
    {
        Autor edautor = autorservice.findAutorById(id);
        model.addAttribute("edautor", edautor);
        return "editAutor";
    }

    @RequestMapping(value = "/edit/autor", method = RequestMethod.POST)
    public String update(@ModelAttribute("autor") Autor upautor, Model model)
    {
        Autor oldAutor = autorservice.findAutorById(upautor.getId());
        oldAutor.setNombre(upautor.getNombre());
        oldAutor.setApellido(upautor.getApellido());

        autorservice.createAutor(oldAutor);
        return "redirect:/books";
    }
}
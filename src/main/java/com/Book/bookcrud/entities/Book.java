package com.Book.bookcrud.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Book
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull
    private String titulo;

    @NotNull
    private String categoria;


    @ManyToOne
    private Autor autorComboBox;

    @NotNull
    @Column(columnDefinition="int(11) default 0")
    private Integer likes=0;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Integer getLikes() { return likes;}

    public void setLikes(Integer likes) {this.likes = likes;}

    public String getCategoria() { return categoria;    }

    public void setCategoria(String categoria) { this.categoria = categoria; }

    public Autor getAutorComboBox() {
        return autorComboBox;
    }

    public void setAutorComboBox(Autor autorComboBox) {
        this.autorComboBox = autorComboBox;
    }
}

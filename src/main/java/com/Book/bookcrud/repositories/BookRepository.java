package com.Book.bookcrud.repositories;

import com.Book.bookcrud.entities.Book;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface BookRepository extends CrudRepository<Book,Integer>
{
}

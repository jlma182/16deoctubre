package com.Book.bookcrud.services;

import com.Book.bookcrud.entities.Autor;
import com.Book.bookcrud.repositories.AutorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AutorServiceImpl implements AutorService
{
    AutorRepository autorRepository;

    @Autowired
    @Qualifier(value = "autorRepository")
    public void setAutorRepository(AutorRepository autorRepository)
    {
        this.autorRepository = autorRepository;
    }

    @Override
    public Iterable<Autor> listAllAutores()
    {
        Iterable<Autor> autores = autorRepository.findAll();
        return autores;
    }

    @Override
    public Autor findAutorById(Integer id)
    {
        Optional<Autor> autor = autorRepository.findById(id);
        return autor.get();
    }

    @Override
    public void createAutor(Autor autor)
    {
        autorRepository.save(autor);
    }

    @Override
    public void deleteAutor(Integer id)
    {
        autorRepository.deleteById(id);
    }

    @Override
    public void saveAutor(Autor vautor)
    {
        autorRepository.save(vautor);
    }

}
